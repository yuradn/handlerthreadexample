package com.example.yuri.handlerthreadexample;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.yuri.handlerthreadexample.loader.MyLoader;
import com.example.yuri.handlerthreadexample.threads.RFragment;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String>{
    private static final String TAG = "MainActivity";
    private MyLoader myLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myLoader = (MyLoader) getSupportLoaderManager().getLoader(MyLoader.ID);
        if (myLoader==null) {
            getSupportLoaderManager().initLoader(MyLoader.ID, null, this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RFragment rFragment = (RFragment) getSupportFragmentManager().findFragmentByTag(RFragment.class.getCanonicalName());
        if (rFragment==null) {
            Log.d(TAG, "NULL! Create new fragment!");
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container,
                            new RFragment(),
                            RFragment.class.getCanonicalName())
                    .commit();
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        switch (id){
            case MyLoader.ID:
                MyLoader loader = new MyLoader(getApplicationContext());
                Log.d(TAG, "onCreateLoader: " + loader.hashCode());
                return loader;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }
}
