package com.example.yuri.handlerthreadexample;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.util.Pair;
import android.util.Log;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by test on 5/17/16.
 */
public class ThumbnailDownloader<T> extends HandlerThread {
    private static final String TAG = "ThumbnailDownloader";
    private static final int MESSAGE_DOWNLOAD = 0;
    private Handler mRequestHandler;
    private Handler mResponseHandler;

    public ThumbnailDownloader() {
        super(TAG);
    }

    public ThumbnailDownloader(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;
    }

    public ThumbnailDownloader(int priority) {
        super(TAG, priority);
    }

    @Override
    protected void onLooperPrepared() {
        mRequestHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_DOWNLOAD) {
                    Pair pair = (Pair) msg.obj;
                    TextView textView = (TextView) pair.first;
                    String url = (String) pair.second;
                    Log.i(TAG, "Got a URL: " + url);
                    int sleep = new Random().nextInt(2000) + 200;
                    Log.d(TAG, "Start sleep, time: " + sleep + " id: " + url);
                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mResponseHandler.post(new MyRunnable(textView, url));
                    Log.d(TAG, "End sleep, id: " + url);
                }
            }
        };
    }

    public void queueThumbnail(T target, String url) {
        Pair pair = new Pair(target, url);
        mRequestHandler.obtainMessage(MESSAGE_DOWNLOAD, pair).sendToTarget();
    }


    class MyRunnable implements Runnable {
        TextView textView;
        String str;

        public MyRunnable(TextView textView, String str) {
            this.textView = textView;
            this.str = str;
        }

        @Override
        public void run() {
            textView.append(str + "\n");
        }
    }
}
