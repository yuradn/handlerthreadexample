package com.example.yuri.handlerthreadexample.threads;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by test on 5/20/16.
 */
public class ReceiverMessages<T> extends HandlerThread {
    private final static String TAG = "ReceiverMessages";
    private Handler mRequestHandler;
    private Handler mResponceHandler;
    private static final int MESSAGE_TYPE_ONE = 1;
    private TextView textView;
    private ArrayList<Item> messages;

    public ReceiverMessages() {
        super(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        start();
        getLooper();
        messages = new ArrayList<>();
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        mRequestHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj instanceof Item) {
                    Item item = (Item) msg.obj;
                    SystemClock.sleep(1000);
                    Log.d(TAG, item.getMessage());
                    if (ReceiverMessages.interrupted() || Thread.interrupted()) {
                        Log.e(TAG, "Interrupted!!!");
                        return;
                    }
                    if (messages != null) messages.add(item);
                    if (mResponceHandler != null) {
                        mResponceHandler.post(new MyRunnable(textView, sortMessages()));
                    }
                }

            }
        };
    }

    private String sortMessages() {
        Collections.sort(messages, new Comparator<Item>() {
            @Override
            public int compare(Item lhs, Item rhs) {
                return lhs.getId() < rhs.getId() ? -1 : (lhs.getId() == rhs.getId() ? 0 : 1);
            }
        });
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < messages.size(); i++) {
            stringBuilder.append(messages.get(i).getMessage()).append("\n");
        }
        return stringBuilder.toString();
    }

    public void addMessage(T object) {
        mRequestHandler.obtainMessage(MESSAGE_TYPE_ONE, object).sendToTarget();
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public void setmResponceHandler(Handler mResponceHandler) {
        this.mResponceHandler = mResponceHandler;
    }

    public void printText() {
        mResponceHandler.post(new MyRunnable(textView, sortMessages()));
    }

    public void clear() {
        if (textView != null) textView.setText("");
        interrupt();
        mRequestHandler.removeMessages(MESSAGE_TYPE_ONE);
        getLooper().quit();
        quit();
        mRequestHandler = null;
        mResponceHandler = null;
        textView = null;
        messages = null;
    }

    public void onStart(TextView textView, Handler handler) {
        setTextView(textView);
        setmResponceHandler(handler);
        printText();
    }

    public void onStop() {
        mResponceHandler = null;
        textView = null;
    }

    class MyRunnable implements Runnable {
        TextView tv;
        String str;

        public MyRunnable(TextView tv, String str) {
            this.tv = tv;
            this.str = str;
        }

        @Override
        public void run() {
            tv.setText(str);
        }
    }
}
