package com.example.yuri.handlerthreadexample.threads;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.yuri.handlerthreadexample.MyApp;
import com.example.yuri.handlerthreadexample.R;

import java.util.Random;

/**
 * Created by test on 5/20/16.
 */
public class RFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "RFragment";
    TextView tvResult;
    Button btnStart;
    Button btnStop;
    Handler mHandler;
    ReceiverMessages receiverMessages;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.thumb_screen, container, false);
        btnStart = (Button) rootView.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
        btnStop = (Button) rootView.findViewById(R.id.btnStop);
        btnStop.setOnClickListener(this);
        tvResult = (TextView) rootView.findViewById(R.id.tvHello);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        receiverMessages = MyApp.getApp(getActivity()).getReceiverMessages();

        if (receiverMessages != null) {
            mHandler = new Handler();
            receiverMessages.onStart(tvResult, mHandler);
        }

    }

    private void letsWork() {
        for (int i = 0; i < 20; i++) {
            new Thread(new MyRunnable(i)).start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        if (receiverMessages!=null) receiverMessages.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                if (receiverMessages == null) {
                    receiverMessages = new ReceiverMessages();
                    mHandler = new Handler();
                    receiverMessages.setmResponceHandler(mHandler);
                    receiverMessages.setTextView(tvResult);
                    receiverMessages.printText();
                    MyApp.getApp(getActivity()).setReceiverMessages(receiverMessages);
                    letsWork();
                }
                break;
            case R.id.btnStop:
                if (receiverMessages!=null) {
                    receiverMessages.clear();
                    receiverMessages=null;
                }
                break;
        }
    }

    class MyRunnable implements Runnable {
        int id;

        public MyRunnable(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            int time = new Random().nextInt(900) + 100;
            SystemClock.sleep(time);
            Item item = new Item(id, "Message id: " + id + " sleep time: " + time, time);
            if (receiverMessages!=null) receiverMessages.addMessage(item);
        }
    }

}
