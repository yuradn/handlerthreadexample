package com.example.yuri.handlerthreadexample;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Unbinder;

/**
 * Created by test on 5/17/16.
 */
public class ThumbFragment extends Fragment {
    private static final String TAG = "ThumbFragment";
    private ThumbnailDownloader mThumbnailDownloader;
    TextView tvHello;
    Button btnStart;

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Handler responseHandler = new Handler();
        mThumbnailDownloader = new ThumbnailDownloader<>(responseHandler);
        mThumbnailDownloader.start();
        mThumbnailDownloader.getLooper();
        Log.i(TAG, "Background thread started");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.thumb_screen, container, false);
        //unbinder = ButterKnife.bind(this, rootView);
        btnStart = (Button) rootView.findViewById(R.id.btnStart);
        tvHello = (TextView) rootView.findViewById(R.id.tvHello);
        return rootView;
    }

    /*@OnClick(R.id.btnStart)
    protected void btnStartClick(){
        Log.d(TAG, "Click to start");
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Click!");
                for (int i = 0; i < 50; i++) {
                    mThumbnailDownloader.queueThumbnail(tvHello, ""+i);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThumbnailDownloader.quit();
    }
}
