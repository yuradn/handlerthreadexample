package com.example.yuri.handlerthreadexample;

/**
 * Created by test on 5/16/16.
 */
public class MyQueueExeption extends Exception {
    public MyQueueExeption(String detailMessage) {
        super(detailMessage);
    }
}
