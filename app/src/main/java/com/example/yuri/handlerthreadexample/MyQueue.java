package com.example.yuri.handlerthreadexample;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by test on 5/16/16.
 */
public class MyQueue {
    private static final String TAG = "MyQueue";
    public static final int OPERATION_ONE = 1;
    public static final String BUNDLE_TEXT = "com.example.yuri.handlerthreadexample.text";
    public static final String BUNDLE_ID = "com.example.yuri.handlerthreadexample.id";

    HandlerThread thread;
    Looper looper;
    Handler handler;
    TextView textView;
    Activity activity;
    long startTime;

    public MyQueue(Activity activity, TextView textView) {
        this.textView = textView;
        this.activity = activity;
    }

    public void start() {
        init();
    }

    public void stop(){
        if (thread!=null) {
            //thread.interrupt();
            //handler.removeMessages(OPERATION_ONE);
            startTime =0;
            thread.interrupt();
            looper.quit();
            thread.quit();
            if (activity!=null && textView!=null) {
                textView.setText("");
            }
        }
    }

    private void init() {
        if (thread!=null) stop();

        startTime = System.currentTimeMillis();
        thread = new HandlerThread("Requests", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        looper = thread.getLooper();
        handler = new Handler(looper) {
            @Override
            public void handleMessage(Message msg) {
                if (thread.isInterrupted()) {
                    return;
                }

                switch(msg.what) {
                    case 0:
                        try {
                            throw new MyQueueExeption("Operation = 0!");
                        } catch (MyQueueExeption myQueueExeption) {
                            myQueueExeption.printStackTrace();
                        }
                        break;
                    case OPERATION_ONE:
                        // SOME_MESSAGE_ID is any int value
                        // do something
                        long mTimeClick = startTime;
                        int id = msg.getData().getInt(BUNDLE_ID, 0);
                        int sleep = new Random().nextInt(2000)+200;
                        Log.d(TAG, "Start sleep, time: "+sleep+" id: "+id);
                        try {
                            Thread.sleep(sleep);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "End sleep, id: "+id);
                        if (mTimeClick!= startTime) return;
                        String text = msg.getData().getString(BUNDLE_TEXT, "");
                        text+="\n";
                        sendText(text);
                        break;
                    // other cases
                }
            }
        };
    }

    private void sendText(String text){
        Log.d(TAG, text);
        activity.runOnUiThread(new MyRun(textView, text));
    }

    public void startWorkInQueue(String text, int id) {
        Log.d(TAG, "StartWorkInQueue, id: "+id);
        Bundle b = new Bundle();
        b.putString(BUNDLE_TEXT, text);
        b.putInt(BUNDLE_ID, id);
        Message message = new Message();
        message.what=OPERATION_ONE;
        message.setData(b);
        handler.sendMessage(message);
    }

    class MyRun implements Runnable {
        String text;
        TextView textView;

        public MyRun(TextView textView, String text) {
            this.text = text;
            this.textView = textView;
        }
        @Override
        public void run() {
            textView.append(text);
        }
    }
}
