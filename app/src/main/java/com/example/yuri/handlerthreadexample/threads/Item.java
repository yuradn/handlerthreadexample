package com.example.yuri.handlerthreadexample.threads;

/**
 * Created by test on 5/20/16.
 */
public class Item {
    private int id;
    private String message;
    private int sleepTime;

    public Item() {
    }

    public Item(int id, String message, int sleepTime) {
        this.id = id;
        this.message = message;
        this.sleepTime = sleepTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }
}
