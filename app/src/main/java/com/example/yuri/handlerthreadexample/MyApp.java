package com.example.yuri.handlerthreadexample;

import android.app.Activity;
import android.app.Application;

import com.example.yuri.handlerthreadexample.threads.ReceiverMessages;

/**
 * Created by test on 5/20/16.
 */
public class MyApp extends Application {
    ReceiverMessages receiverMessages=null;

    public static MyApp getApp(Activity context){
        return ((MyApp)context.getApplication());
    }

    public ReceiverMessages getReceiverMessages() {
        return receiverMessages;
    }

    public void setReceiverMessages(ReceiverMessages receiverMessages) {
        this.receiverMessages = receiverMessages;
    }
}
